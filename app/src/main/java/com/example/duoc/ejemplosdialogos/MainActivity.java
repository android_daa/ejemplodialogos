package com.example.duoc.ejemplosdialogos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends BaseActivity {

    private Button btnAlertDialog;
    private Button btnListDialog;
    private CharSequence [] colores = {"Rojo", "Verde", "amarillo"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAlertDialog = (Button) findViewById(R.id.btn_alert_dialog);
        btnAlertDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*MiDialogFragment m = new MiDialogFragment();
                m.show(getFragmentManager(),"");*/
                mostrarConfirmDialog("Este es el titulo", "Este es el mensaje", "OK", "Cancel", new OnClickConfirmDialogListener() {
                    @Override
                    public void onClickAceptar() {

                    }

                    @Override
                    public void onClickCancelar() {

                    }
                });
            }
        });

        btnListDialog = (Button)findViewById(R.id.btn_list_dialog);
        btnListDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarListDialog("Titulo list",
                        colores
                        );
            }
        });
    }


}
