package com.example.duoc.ejemplosdialogos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by DUOC on 06-05-2017.
 */

public class BaseActivity extends AppCompatActivity {

    /*protected void mostrarDialog(String titulo,
                                 String mensaje,
                                 String aceptarText,
                                 String cancelarText,
                                 DialogInterface.OnClickListener aceptarListener,
                                 DialogInterface.OnClickListener cancelarListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.
                setTitle(titulo).
                setMessage(mensaje)
                .setPositiveButton(aceptarText, aceptarListener)
                .setNegativeButton(cancelarText, cancelarListener)
        ;
        // Create the AlertDialog object and return it
        builder.create().show();
    }*/

    protected void mostrarConfirmDialog(String titulo,
                                 String mensaje,
                                 String aceptarText,
                                 String cancelarText,
                                 final OnClickConfirmDialogListener onClickConfirmDialog) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.
                setTitle(titulo).
                setMessage(mensaje)
                .setPositiveButton(aceptarText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onClickConfirmDialog.onClickAceptar();
                    }
                })
                .setNegativeButton(cancelarText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onClickConfirmDialog.onClickCancelar();
                    }
                })
        ;
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    protected void mostrarListDialog(String titulo,
                                     final CharSequence[] items){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo)
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        Toast.makeText(BaseActivity.this, "Item Seleccionado: " + items[which], Toast.LENGTH_SHORT).show();
                    }
                });
        builder.create().show();
    }

    public interface OnClickConfirmDialogListener{
        void onClickAceptar();
        void onClickCancelar();
    }
}
